//
//  AppDelegate.swift
//  WindmillComic
//
//  Created by Ziyi Zhang on 15/05/2017.
//  Copyright © 2017 Ziyideas. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, URLSessionDelegate {

  var window: UIWindow?
  lazy var coreDataStack = CoreDataStack(modelName: "Comics")
  
  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey : Any]? = nil) -> Bool {
    guard let newestComicViewController = window?.rootViewController as? NewestComicViewController else { return true }
    
    newestComicViewController.managedContext = coreDataStack.managedContext
    
    DownloadManager.shared.managedContext = coreDataStack.managedContext
    DownloadManager.shared.checkAndDownload()
    
    return true
  }
  
  func applicationDidBecomeActive(_ application: UIApplication) {
    DownloadManager.shared.checkAndDownload()
  }
  
  func applicationWillResignActive(_ application: UIApplication) {
    DownloadManager.shared.pauseAllDownload()
  }

  func applicationDidEnterBackground(_ application: UIApplication) {
    coreDataStack.saveContext()
  }
  
  func applicationWillEnterForeground(_ application: UIApplication) {
    DownloadManager.shared.checkAndDownload()
  }
  
  func applicationWillTerminate(_ application: UIApplication) {
    DownloadManager.shared.pauseAllDownload()
  }
  

}
