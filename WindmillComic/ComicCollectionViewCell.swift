//
//  ComicCollectionViewCell.swift
//  WindmillComic
//
//  Created by Ziyi Zhang on 15/05/2017.
//  Copyright © 2017 Ziyideas. All rights reserved.
//

import UIKit
import CoreData

import Kingfisher
import Spring
import ChameleonFramework

class ComicCollectionViewCell: UICollectionViewCell {
  @IBOutlet var comicCoverImageView: UIImageView!
  @IBOutlet var comicTitle: UILabel!
  @IBOutlet var yearLabel: UILabel!
  @IBOutlet var comicEpisodeCount: UILabel!
  @IBOutlet var starButton: DesignableButton!
  
  var comicID: String = ""
  
  var isStared = false {
    didSet {
      isStared ? starButton.setImage(#imageLiteral(resourceName: "stared-button"), for: .normal) : starButton.setImage(#imageLiteral(resourceName: "star-button"), for: .normal)
    }
  }
  
  private var colorFromImage = UIColor() {
    didSet {
      comicCoverImageView.layer.shadowOffset = CGSize(width: 0, height: 2)
      comicCoverImageView.layer.shadowRadius = 4.0
      comicCoverImageView.layer.shadowOpacity = 0.5
      comicCoverImageView.layer.shadowColor = colorFromImage.cgColor
      
      comicTitle.textColor = colorFromImage
    }
  }
  
  func setup(comicOverview: ComicOverView) {
    if let coverURL = URL(string: comicOverview.coverURL!) {
      comicCoverImageView.kf.setImage(with: coverURL, options: [.transition(.fade(0.2))]) { (image, error, cacheType, imageUrl) in
        if let image = image {
          self.colorFromImage = AverageColorFromImage(image)
        } else {
          self.colorFromImage = .black
        }
      }
    }
    
    comicTitle.text = comicOverview.title
    yearLabel.text = comicOverview.year
    comicEpisodeCount.text = "共 \(comicOverview.totalEpisodes!) 集"
      
    comicID = comicOverview.id!
  }
  
  func setup(comicManagedObject: StaredComic) {
    let coverImage = UIImage(data: comicManagedObject.cover! as Data)
    comicCoverImageView.image = coverImage
    
    comicTitle.text = comicManagedObject.title
    yearLabel.text = comicManagedObject.year
    comicEpisodeCount.text = comicManagedObject.totalEpisodes
    comicID = comicManagedObject.id!
  }
}
