//
//  NetworkService.swift
//  WindmillComic
//
//  Created by Ziyi Zhang on 17/05/2017.
//  Copyright © 2017 Ziyideas. All rights reserved.
//

import PromiseKit
import Alamofire

class NetworkService {
  static func getJson(with url: String, method: HTTPMethod = .get, parameters: Parameters? = nil, headers: [String: String]? = nil) -> Promise<NSDictionary> {
    return Promise { fulfill, reject in
      Alamofire.request(url, parameters: parameters, headers: headers).responseJSON() { response in
        switch response.result {
        case .success(let dict):
          fulfill(dict as! NSDictionary)
        case .failure(let error):
          reject(error)
        }
      }
    }
  }
}
