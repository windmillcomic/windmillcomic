//
//  EpisodeCollectionViewCell.swift
//  WindmillComic
//
//  Created by Ziyi Zhang on 16/05/2017.
//  Copyright © 2017 Ziyideas. All rights reserved.
//

import UIKit

import Spring
import SnapKit
import ChameleonFramework

class EpisodeCollectionViewCell: UICollectionViewCell {
  @IBOutlet var episodeButton: DesignableButton!
  @IBOutlet var episodeDownloadSelectionButton: DesignableButton!
  @IBOutlet var downloadedEpisodeButton: DesignableButton!
  @IBOutlet weak var deleteOverlayButton: DesignableButton!
  @IBOutlet weak var pauseView: UIView!
  
  lazy var chartProgress: RPCircularProgress = {
    let progress = RPCircularProgress()
    progress.roundedCorners = false
    progress.thicknessRatio = 1
    progress.trackTintColor = HexColor("#CEA6F1", 0.4)!
    return progress
  }()
  
  func setup(number: Int, episodeNumber: Int16? = nil, with identifier: String) {
    if identifier == "FromDownloadSelectionViewController" {
      DispatchQueue.main.async {
        self.episodeDownloadSelectionButton.setTitle("\(number + 1)", for: .normal)
      }
    } else if identifier == "FromDownloadViewController" {
      DispatchQueue.main.async {
        self.pauseView.alpha = 0
        
        self.downloadedEpisodeButton.setTitle("\(episodeNumber! + 1)", for: .normal)
      }
      
      if chartProgress.progress != 1.0 || (chartProgress.superview == nil) {
        downloadedEpisodeButton.addSubview(chartProgress)
        
        chartProgress.snp.makeConstraints { make -> Void in
          make.edges.equalTo(self)
        }
      } else {
        chartProgress.removeFromSuperview()
      }
    } else {
      DispatchQueue.main.async {
        self.episodeButton.setTitle("\(number + 1)", for: .normal)
      }
    }
  }
}
