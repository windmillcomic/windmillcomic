//
//  NewestComicViewController.swift
//  WindmillComic
//
//  Created by Ziyi Zhang on 15/05/2017.
//  Copyright © 2017 Ziyideas. All rights reserved.
//

import UIKit
import CoreData

import Spring
import ChameleonFramework
import LemonDeer
import Device
import Reachability

class NewestComicViewController: UIViewController {
  @IBOutlet var titleBackground: DesignableImageView!
  @IBOutlet var pageTitle: UILabel!
  
  @IBOutlet var comicCollectionView: UICollectionView!
  @IBOutlet var downloadNavButton: DesignableButton!
  @IBOutlet var downloadProgressLabel: UILabel!
  @IBOutlet var starNavButton: DesignableButton!
  @IBOutlet var starChangeLabel: UILabel!
  @IBOutlet var searchBackgroundViewButton: DesignableButton!
  @IBOutlet var searchButton: UIButton!
  @IBOutlet var searchTextField: DesignableTextField!
  @IBOutlet var cancelSearchButton: UIButton!
  @IBOutlet var networkCheckStackView: UIStackView!
  
  @IBOutlet var backView: DesignableView!
  @IBOutlet var backViewBottomConstraint: NSLayoutConstraint!
  @IBOutlet var collectionViewBottomConstraint: NSLayoutConstraint!
  
  @IBOutlet weak var warningView: UIView!
  @IBOutlet weak var warningLabel: UILabel!
  
  fileprivate var comicOverviewViewModel = ComicOverviewViewModel()
  fileprivate var currentPage = 1
  fileprivate var staredComicsShowed = false {
    didSet {
      if staredComicsShowed {
        comicOverviewViewModel.staredComicsShowed = true
        
        starNavButton.borderWidth = 2.0
        starNavButton.borderColor = HexColor("#FC387E")!
        starNavButton.backgroundColor = UIColor.white
        starNavButton.setImage(#imageLiteral(resourceName: "stared"), for: .normal)
        
        titleBackground.backgroundColor = starNavButton.borderColor
        pageTitle.text = "收藏的动漫"
        pageTitle.textColor = HexColor("#C70754")
        
        backView.alpha = 1
        collectionViewBottomConstraint.constant = 60
        
        scrollToTop()
        
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {
          self.view.layoutIfNeeded()
        })
      } else {
        comicOverviewViewModel.staredComicsShowed = false
        
        starNavButton.borderColor = UIColor.clear
        starNavButton.backgroundColor = HexColor("#FC387E")
        starNavButton.setImage(#imageLiteral(resourceName: "star"), for: .normal)
        
        setDefaultTitle()
        
        backView.alpha = 0
        collectionViewBottomConstraint.constant = 0
        
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseIn, animations: {
          self.view.layoutIfNeeded()
        })
      }
    }
  }
  
  var downloadComicsShowed = false {
    didSet {
      if downloadComicsShowed {
        staredComicsShowed = false
        comicOverviewViewModel.searchedComicsShowed = false
        
        DispatchQueue.main.async {
          self.downloadProgressLabel.alpha = 0
          self.downloadNavButton.alpha = 0
          self.titleBackground.backgroundColor = HexColor("#7586F0")
          self.pageTitle.text = "下载的动漫"
          self.pageTitle.textColor = HexColor("#2D3697")
        }

        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {
          self.view.layoutIfNeeded()
        })
      } else {
        setDefaultTitle()
        
        comicCollectionView.reloadData()
        
        DispatchQueue.main.async {
          self.downloadProgressLabel.alpha = 1
          
          let progressPercentage = Int(DownloadManager.shared.downloadingProgress * 100)
          
          if progressPercentage == 0 || progressPercentage == 100 {
            self.downloadProgressLabel.text = ""
          } else {
            self.downloadProgressLabel.text = "\(progressPercentage) %"
          }
          
          self.downloadNavButton.alpha = 1
        }
        
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseIn, animations: {
          self.view.layoutIfNeeded()
        })
      }
    }
  }
  
  lazy var coreDataStack = CoreDataStack(modelName: "Comics")
  var managedContext: NSManagedObjectContext!
  
  let reachability = Reachability()!
  let refreshControl  = UIRefreshControl()
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    
    NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged(note:)), name: .reachabilityChanged, object: reachability)
    do{
      try reachability.startNotifier()
    }catch{
      print("could not start reachability notifier")
    }
  }
  
  @objc func reachabilityChanged(note: Notification) {
    let reachability = note.object as! Reachability
    
    switch reachability.connection {
    case .wifi, .cellular:
      setupRefreshControl()
    case .none:
      print("No network connection")
    }
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    NotificationCenter.default.addObserver(self, selector: #selector(updateProgress), name: Constants.NotificationName.DownloadProgressNotification, object: nil)
    
    setDefaultTitle()

    backView.alpha = 0
    networkCheckStackView.alpha = 0
    collectionViewBottomConstraint.constant = 0
    
    comicOverviewViewModel.managedContext = managedContext
    
    refresh(sender: refreshControl)
    
    comicOverviewViewModel.comicManagedObjects = fetchStaredComics()
  }
  
  @IBAction func starNavButtonDidPressed(_ sender: Any) {
    if comicOverviewViewModel.comicManagedObjects.count == 0 {
      showWarning(with: "还没有收藏任何动漫", HexColor("#C31F55")!, HexColor("#FC387E")!)
    } else {
      staredComicsShowed = !staredComicsShowed
      restoreSearchView()
      comicOverviewViewModel.searchedComicsShowed = false
      
      comicCollectionView.reloadData()
      
      scrollToTop()
    }
  }
  
  @IBAction func downloadNavButtonDidPressed(_ sender: Any) {
    downloadComicsShowed = true
    
    DispatchQueue.main.async {
      self.downloadProgressLabel.alpha = 0
    }
    
    restoreSearchView()
  }
  
  @IBAction func searchBackgroundViewButtonDidPressed(_ sender: Any) {
    cancelSearchButton.isUserInteractionEnabled = true
    
    searchButton.isUserInteractionEnabled = true
    
    searchTextField.text = ""
    
    UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 0, options: .curveEaseIn, animations: {
      self.searchButton.transform = CGAffineTransform(translationX: -self.searchBackgroundViewButton.frame.width * 3,
                                                         y: self.searchBackgroundViewButton.frame.height * 4)
      self.searchTextField.transform = CGAffineTransform(translationX: -self.searchBackgroundViewButton.frame.width * 3,
                                                         y: self.searchBackgroundViewButton.frame.height * 2.6)
      self.searchBackgroundViewButton.transform = CGAffineTransform(scaleX: 6, y: 6).translatedBy(x: -20, y: 20)
      
      self.searchTextField.alpha = 1
    })
  }
  
  @IBAction func cancelSearchButtonDidPressed(_ sender: Any) {
    restoreSearchView()
  }
  
  @IBAction func searchButtonDidPressed(_ sender: Any) {
    search()
  }
  
  @IBAction func backButtonDidPressed(_ sender: Any) {
    staredComicsShowed = false
    comicOverviewViewModel.staredComicsShowed = false
    comicOverviewViewModel.searchedComicsShowed = false
    
    restoreSearchView()
    
    comicCollectionView.reloadData()
  }
  
  @IBAction func starButtonDidPressed(_ sender: Any) {
    guard let selectedIndexPath = getCurrentCellIndexPath(sender: sender),
      let cell = comicCollectionView.cellForItem(at: selectedIndexPath) as? ComicCollectionViewCell else { return }
    
    if cell.isStared {
      cell.isStared = false
      
      DispatchQueue.main.async {
        self.starChangeLabel.alpha = 0
        self.starChangeLabel.transform = CGAffineTransform(translationX: 0, y: -10)
        self.starChangeLabel.text = "-1"
        
        UIView.animate(withDuration: 0.5, animations: {
          self.starChangeLabel.alpha = 1
          self.starChangeLabel.transform = CGAffineTransform.identity
        }, completion: { _ in
          delay(delay: 1) {
            UIView.animate(withDuration: 0.3, animations: {
              self.starChangeLabel.alpha = 0
              self.starChangeLabel.transform = CGAffineTransform(translationX: 0, y: 10)
            })
          }
        })
      }
      
      let staredComicFetchRequest: NSFetchRequest<StaredComic> = StaredComic.fetchRequest()
      staredComicFetchRequest.predicate = NSPredicate(format: "id = %@", cell.comicID)
      
      do {
        let result = (try managedContext.fetch(staredComicFetchRequest) as [StaredComic])[0]
        
        managedContext.delete(result)
        comicOverviewViewModel.comicManagedObjects = fetchStaredComics()
      } catch let error as NSError {
        print("Unresolved error for saving to managed context: \(error), \(error.userInfo)")
      }
    } else {
      cell.isStared = true
      
      DispatchQueue.main.async {
        self.starChangeLabel.alpha = 0
        self.starChangeLabel.transform = CGAffineTransform(translationX: 0, y: 10)
        self.starChangeLabel.text = "+1"
        
        UIView.animate(withDuration: 0.5, animations: {
          self.starChangeLabel.alpha = 1
          self.starChangeLabel.transform = CGAffineTransform.identity
        }, completion: { _ in
          delay(delay: 1) {
            UIView.animate(withDuration: 0.3, animations: {
              self.starChangeLabel.alpha = 0
              self.starChangeLabel.transform = CGAffineTransform(translationX: 0, y: -10)
            })
          }
        })
      }
      
      let entity = NSEntityDescription.entity(forEntityName: "StaredComic", in: managedContext)!
      let staredComic = StaredComic(entity: entity, insertInto: managedContext)
      staredComic.id = cell.comicID
      staredComic.title = cell.comicTitle.text
      staredComic.year = cell.yearLabel.text
      staredComic.totalEpisodes = cell.comicEpisodeCount.text
      let coverData = NSData(data: UIImagePNGRepresentation(cell.comicCoverImageView.image!)!)
      staredComic.cover = coverData as Data
      let staredDate = NSDate()
      staredComic.date = staredDate as Date
      
      coreDataStack.saveContext()
      
      comicOverviewViewModel.comicManagedObjects = fetchStaredComics()
    }
    
    cell.starButton.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
    
    UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0, options: .curveEaseOut, animations: {
      cell.starButton.transform = .identity
    })
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    var selectedComicID = ""
    var selectedComicTitle = ""
    
    if let selectedRow = getCurrentCellIndexPath(sender: sender)?.row {
      if staredComicsShowed {
        selectedComicID = comicOverviewViewModel.comicManagedObjects[selectedRow].id!
        selectedComicTitle = comicOverviewViewModel.comicManagedObjects[selectedRow].title!
      } else if comicOverviewViewModel.searchedComicsShowed {
        selectedComicID = comicOverviewViewModel.searchedComicOverviews[selectedRow].id!
        selectedComicTitle = comicOverviewViewModel.searchedComicOverviews[selectedRow].title!
      } else {
        selectedComicID = comicOverviewViewModel.comicOverviews[selectedRow].id!
        selectedComicTitle = comicOverviewViewModel.comicOverviews[selectedRow].title!
      }
    }
    
    if segue.identifier == "ShowEpisodes" {
      if let destinationViewController = segue.destination as? EpisodeViewController {
        destinationViewController.id = selectedComicID
        destinationViewController.comicTitle = selectedComicTitle
      }
    } else if segue.identifier == "ShowDownloadSelection" {
      if let destinationViewController = segue.destination as? DownloadSelectionViewController {
        destinationViewController.id = selectedComicID
        destinationViewController.comicTitleFromPreviousVC = selectedComicTitle
        
        guard let selectedCell = comicCollectionView.cellForItem(at: getCurrentCellIndexPath(sender: sender)!) as? ComicCollectionViewCell else { return }
        
        destinationViewController.comicCoverFromPreviousVC = selectedCell.comicCoverImageView.image
        destinationViewController.managedContext = managedContext
      }
    } else if segue.identifier == "ShowDownloadedComics" {
      if let destinationViewController = segue.destination as? DownloadViewController {
        destinationViewController.managedContext = managedContext
      }
    }
  }
  
  private func getCurrentCellIndexPath(sender: Any?) -> IndexPath? {
    if let sourceSender = sender as? UIButton {
      let buttonPosition = sourceSender.convert(CGPoint.zero, to: comicCollectionView)
      let selectedIndexPath = comicCollectionView.indexPathForItem(at: buttonPosition)
      
      return selectedIndexPath
    }
    
    return nil
  }
  
  private func fetchStaredComics() -> [StaredComic] {
    let staredComicFetchRequest: NSFetchRequest<StaredComic> = StaredComic.fetchRequest()
    let sort = NSSortDescriptor(key: "date", ascending: false)
    staredComicFetchRequest.sortDescriptors = [sort]
    
    var results = [StaredComic]()
    
    do {
      results = try managedContext.fetch(staredComicFetchRequest) as [StaredComic]
    } catch let error as NSError {
      print("Unresolved error for saving to managed context: \(error), \(error.userInfo)")
    }
    
    return results
  }
  
  private func setDefaultTitle() {
    titleBackground.backgroundColor = HexColor("#FF906B")
    pageTitle.text = "最新动漫"
    pageTitle.textColor = HexColor("#D54708")
  }
  
  fileprivate func restoreSearchView() {
    searchButton.isUserInteractionEnabled = false
    cancelSearchButton.isUserInteractionEnabled = false
    
    UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseOut, animations: {
      self.searchBackgroundViewButton.transform = .identity
      self.searchButton.transform = .identity
      self.searchTextField.transform = .identity
      self.searchTextField.alpha = 0
    })
  }
  
  fileprivate func search() {
    staredComicsShowed = false
    comicOverviewViewModel.staredComicsShowed = false
    
    searchTextField.resignFirstResponder()
    
    restoreSearchView()
    
    guard let searchText = searchTextField.text else {
      return
    }

    if searchText.count > 0 {
      comicOverviewViewModel.getSearchedComics(with: searchText) {
        self.comicOverviewViewModel.searchedComicsShowed = true
        
        self.comicCollectionView.reloadData()
        
        if self.comicOverviewViewModel.searchedComicOverviews.count == 0 {
          self.showWarning(with: "没有搜索到内容", HexColor("#AFA52D")!, HexColor("#FFF24B")!)
        }
      }
      
      scrollToTop()
      
      titleBackground.backgroundColor = self.searchBackgroundViewButton.backgroundColor
      pageTitle.text = "搜索的动漫"
      pageTitle.textColor = HexColor("#DCD100")
      
      backView.alpha = 1
      collectionViewBottomConstraint.constant = 60
      
      UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {
        self.view.layoutIfNeeded()
      })
    }
  }
  
  private func scrollToTop() {
    comicCollectionView.contentOffset.y = 0
  }
  
  private func showWarning(with message: String, _ messageColor: UIColor, _ warningColor: UIColor) {
    warningLabel.text = message
    warningLabel.textColor = messageColor
    
    UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseOut, animations: {
      self.warningView.alpha = 1
    }, completion: { _ in
      delay(delay: 1.5) {
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseIn, animations: {
          self.warningView.alpha = 0
        })
      }
    })
  }
  
  @objc func updateProgress(_ notification: Notification) {
    if downloadComicsShowed { return }
    
    if DownloadManager.shared.downloadingProgress == 0 {
      downloadProgressLabel.text = ""
      
      return
    }
    
    if let directoryName = notification.userInfo?["directoryName"] as? String {
      let progressPercentage = Int(DownloadManager.shared.downloadingProgress * 100)
      
      DispatchQueue.main.async {
        self.downloadProgressLabel.alpha = 1
        self.downloadProgressLabel.transform = CGAffineTransform.identity
        self.downloadProgressLabel.font = self.downloadProgressLabel.font.withSize(14)
        self.downloadProgressLabel.text = "\(progressPercentage) %"
      }
      
      if DownloadManager.shared.downloadingProgress == 1.0 {
        DispatchQueue.main.async {
          self.downloadProgressLabel.alpha = 0
          self.downloadProgressLabel.transform = CGAffineTransform(translationX: 0, y: 10)
          self.downloadProgressLabel.font = self.downloadProgressLabel.font.withSize(10)
          self.downloadProgressLabel.text = "「\(directoryName)」下载完成"
          
          UIView.animate(withDuration: 1, animations: {
            self.downloadProgressLabel.alpha = 1
            self.downloadProgressLabel.transform = CGAffineTransform.identity
          }, completion: { _ in
            delay(delay: 1) {
              UIView.animate(withDuration: 0.3, animations: {
                self.downloadProgressLabel.alpha = 0
                self.downloadProgressLabel.transform = CGAffineTransform(translationX: 0, y: -10)
              })
            }
          })
        }
      }
    }
  }
  
  private func setupRefreshControl() {
    let attributes = [
      NSAttributedStringKey.foregroundColor: HexColor("#4259E7")!,
      NSAttributedStringKey.font: UIFont.systemFont(ofSize: 12.0)
    ]
    refreshControl.attributedTitle = NSAttributedString(string: "正在获取最新动漫", attributes: attributes)
    refreshControl.tintColor = HexColor("#4259E7")!
    
    refreshControl.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
    
    refreshControl.addTarget(self, action: #selector(refresh(sender:)), for: .valueChanged)
    
    if #available(iOS 10.0, *) {
      comicCollectionView.refreshControl = refreshControl
    } else {
      if refreshControl.superclass == nil {
        comicCollectionView.addSubview(refreshControl)
      }
    }
  }
  
  @objc func refresh(sender: UIRefreshControl) {
    setupRefreshControl()
    comicOverviewViewModel.getComics {
      if self.networkCheckStackView.alpha == 1 {
        self.networkCheckStackView.alpha = 0
      }
      
      sender.endRefreshing()
      
      self.comicCollectionView.reloadData()
    }
    
    reachability.whenUnreachable = { _ in
      if #available(iOS 10.0, *) {
        self.comicCollectionView.refreshControl = nil
      } else {
        self.refreshControl.removeFromSuperview()
      }

      if self.comicCollectionView.numberOfItems(inSection: 0) == 0 {
        self.networkCheckStackView.alpha = 1
      }
    }
  }
  
  deinit {
    NotificationCenter.default.removeObserver(self)
  }
}

extension NewestComicViewController: UICollectionViewDataSource {
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    if staredComicsShowed {
      return comicOverviewViewModel.comicManagedObjects.count
    } else if comicOverviewViewModel.searchedComicsShowed {
      return comicOverviewViewModel.searchedComicOverviews.count
    } else {
      return comicOverviewViewModel.comicOverviews.count
    }
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    return comicOverviewViewModel.cellInstance(collectionView, cellForItemAt: indexPath)
  }
}

extension NewestComicViewController: UICollectionViewDelegate {
  func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
    if indexPath.row == comicOverviewViewModel.comicOverviews.count - 1 {
      currentPage += 1
      
      comicOverviewViewModel.getComics(on: currentPage) {
        self.comicCollectionView.reloadData()
      }
    }
  }
}

extension NewestComicViewController: UICollectionViewDelegateFlowLayout {
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    switch Device.type() {
    case .iPad:
      return CGSize(width: comicCollectionView.frame.width / 2 - 20, height: 182)
    default:
      return CGSize(width: comicCollectionView.frame.width, height: 182)
    }
  }
}

extension NewestComicViewController: UITextFieldDelegate {
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    search()
    return true
  }
}
