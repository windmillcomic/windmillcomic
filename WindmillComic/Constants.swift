//
//  Constants.swift
//  WindmillComic
//
//  Created by Ziyi Zhang on 17/05/2017.
//  Copyright © 2017 Ziyideas. All rights reserved.
//

import Foundation

struct Constants {
  static let Base = "http://taobao.jszks.net/index.php"
//  static let Newest = "/Dongman/shenhe/p/"
  static let Newest = "/Dongman/dongmanhuayuan/p/"
  
//  static let Detail = "/Dongman/xiangxi"
  static let Detail = "/Dongman/huayuanxiangxi"
  static let Video = "/Json/newhanju"
  static let Search = "/Json/appsou/type/dongman/name/"
  
  struct NotificationName {
    static let DownloadProgressNotification = NSNotification.Name(rawValue: "UpdateDownloadProgress")
  }
}
