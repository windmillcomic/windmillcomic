//
//  EpisodeViewController.swift
//  WindmillComic
//
//  Created by Ziyi Zhang on 16/05/2017.
//  Copyright © 2017 Ziyideas. All rights reserved.
//

import UIKit

import BMPlayer
import ChameleonFramework

class EpisodeViewController: UIViewController {
  @IBOutlet var episodeCollectionView: UICollectionView!
  @IBOutlet var player: BMPlayer!
  
  var id = ""
  var comicTitle = ""
  
  fileprivate var episodeDetailViewModel = EpisodeDetailViewModel()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    configurePlayer()
    
    episodeDetailViewModel.getEpisodes(with: id) {
      self.episodeCollectionView.reloadData()
      
      _ = NetworkService.getJson(with: Constants.Base + Constants.Video, parameters: [
        "m3u8": self.episodeDetailViewModel.episodes[0].m3u8!,
        "url": self.episodeDetailViewModel.episodes[0].playURL!
        ]).then { dict -> Void in
          print(dict)
          if let videoURL = dict["url"] as? String {
            print(videoURL)
            let resource = BMPlayerResource(url: URL(string: videoURL)!, name: self.comicTitle)
            self.player.setVideo(resource: resource)
            
            if let firstCell = self.episodeCollectionView.cellForItem(at: IndexPath(item: 0, section: 0)) as? EpisodeCollectionViewCell {
              firstCell.episodeButton.backgroundColor = HexColor("#D19CFF")
              firstCell.episodeButton.borderColor = HexColor("#D19CFF")!
              firstCell.episodeButton.setTitleColor(UIColor.white, for: .normal)
            }
          }
      }
    }
    
    player.snp.makeConstraints { make in
      make.top.equalTo(view.snp.top)
      make.left.equalTo(view.snp.left)
      make.right.equalTo(view.snp.right)
      make.height.equalTo(view.snp.width).multipliedBy(9.0/16.0)
    }
    
    episodeCollectionView.snp.makeConstraints { make in
      make.top.equalTo(player.snp.bottom)
      make.left.equalTo(view.snp.left)
      make.right.equalTo(view.snp.right)
      make.bottom.equalTo(view.snp.bottom)
    }
  }
  
  func configurePlayer() {
    BMPlayerConf.loaderType = .ballGridBeat
    BMPlayerConf.tintColor = HexColor("#6B7EE9")!
    
    player.backBlock = { isFullScreen in
      if !isFullScreen {
        self.dismiss(animated: true)
      }
    }
  }
}

extension EpisodeViewController: UICollectionViewDataSource {
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return episodeDetailViewModel.episodes.count
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    return episodeDetailViewModel.cellInstance(collectionView, cellForItemAt: indexPath, with: "FromEpisodeViewController")
  }
}

extension EpisodeViewController: UICollectionViewDelegate {
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    let selectedEpisode = episodeDetailViewModel.episodes[indexPath.row]
    
    if indexPath.row != 0 {
      if let firstCell = self.episodeCollectionView.cellForItem(at: IndexPath(item: 0, section: 0)) as? EpisodeCollectionViewCell {
        firstCell.episodeButton.backgroundColor = UIColor.white
        firstCell.episodeButton.borderColor = HexColor("#D19CFF")!
        firstCell.episodeButton.setTitleColor(HexColor("#D19CFF"), for: .normal)
      }
    }
    
    if let cell = episodeCollectionView.cellForItem(at: indexPath) as? EpisodeCollectionViewCell {
      cell.episodeButton.backgroundColor = HexColor("#D19CFF")
      cell.episodeButton.borderColor = HexColor("#D19CFF")!
      cell.episodeButton.setTitleColor(UIColor.white, for: .normal)
    }
    
    _ = NetworkService.getJson(with: Constants.Base + Constants.Video, parameters: [
      "m3u8": selectedEpisode.m3u8!,
      "url": selectedEpisode.playURL!
      ]).then { dict -> Void in
        let videoURL = dict["url"] as! String
        
        BMPlayerConf.shouldAutoPlay = true
        
        let resource = BMPlayerResource(url: URL(string: videoURL)!, name: self.comicTitle)
        self.player.setVideo(resource: resource)
    }
  }
  
  func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
    guard let cell = episodeCollectionView.cellForItem(at: indexPath) as? EpisodeCollectionViewCell else { return }
    cell.episodeButton.backgroundColor = UIColor.white
    cell.episodeButton.borderColor = HexColor("#D19CFF")!
    cell.episodeButton.setTitleColor(HexColor("#D19CFF"), for: .normal)
  }
}
