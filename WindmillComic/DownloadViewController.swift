//
//  DownloadViewController.swift
//  WindmillComic
//
//  Created by Ziyi Zhang on 15/05/2017.
//  Copyright © 2017 Ziyideas. All rights reserved.
//

import UIKit
import CoreData

import LemonDeer
import BMPlayer
import GCDWebServer
import Spring
import ChameleonFramework

class DownloadViewController: UIViewController {
  @IBOutlet var downloadedComicsTableView: UITableView!
  @IBOutlet weak var warningView: UIView!
  @IBOutlet weak var leftWarningView: DesignableView!
  @IBOutlet weak var warningLabel: UILabel!
  @IBOutlet weak var rightWarningView: DesignableView!
  
  fileprivate var downloadedComics = [DownloadedComic]()
  fileprivate var isDeleteEpisodesButtonActivated = false
  
  lazy var coreDataStack = CoreDataStack(modelName: "Comics")
  var managedContext: NSManagedObjectContext!
  
  var storedOffsets = [Int: CGFloat]()
  
  fileprivate var server: GCDWebServer?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    downloadedComicsTableView.rowHeight = UITableViewAutomaticDimension
    downloadedComicsTableView.estimatedRowHeight = 120
    
    NotificationCenter.default.addObserver(self, selector: #selector(updateProgress), name: Constants.NotificationName.DownloadProgressNotification, object: nil)
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    
    fetchDataAndReload()
  }
  
  @IBAction func backButtonDidPressed(_ sender: Any) {
    guard let presentingViewController = self.presentingViewController as? NewestComicViewController else {
      print("Wrong parent view controller.")
      return
    }
    
    presentingViewController.downloadComicsShowed = false
    
    self.dismiss(animated: false)
  }

  @IBAction func deleteButtonDidPressed(_ sender: Any) {
    isDeleteEpisodesButtonActivated = true
    
    let selectedIndexPath = getCurrentCellIndexPath(sender: sender)!
    
    downloadedComicsTableView.reloadRows(at: [selectedIndexPath], with: .none)
  }
  
  @IBAction func deleteDoneButtonDidPressed(_ sender: Any) {
    isDeleteEpisodesButtonActivated = false
    
    let selectedIndexPath = getCurrentCellIndexPath(sender: sender)!
    guard let selectedCell = downloadedComicsTableView.cellForRow(at: selectedIndexPath) as? DownloadComicTableViewCell else { return }
    
    UIView.animate(withDuration: 0.3, animations: {
      selectedCell.deleteAllButton.alpha = 0
      selectedCell.deleteAllButton.transform = CGAffineTransform(rotationAngle: .pi / 2)
      selectedCell.deleteAllButtonConstraint.constant = 20
      
      self.view.layoutIfNeeded()
      
      UIView.animate(withDuration: 0.3, animations: {
        selectedCell.deleteDoneButton.alpha = 0
        selectedCell.deleteDoneButton.transform = CGAffineTransform(rotationAngle: .pi / 4)
      }, completion: { _ in
        selectedCell.deleteButton.alpha = 1
        
        self.fetchDataAndReload()
        
        DownloadManager.shared.checkAndDownload()
      })
    })
  }
  
  @IBAction func deleteAllButtonDidPressed(_ sender: Any) {
    guard let selectedIndexPath = self.getCurrentCellIndexPath(sender: sender),
      let selectedCell = downloadedComicsTableView.cellForRow(at: selectedIndexPath) as? DownloadComicTableViewCell else {
        return
    }
    
    let comicTitle = self.downloadedComics[selectedIndexPath.row].title!
    
    alert(message: "要删除「\(comicTitle)」整部动漫吗？", title: "", confirmActionTitle: "删除", confirmStyle: .destructive) {
      guard let episodesCount = self.downloadedComics[selectedIndexPath.row].episodes?.count else { return }
      
      for i in 0 ..< episodesCount {
        guard let episode = self.downloadedComics[selectedIndexPath.row].episodes?[i] as? DownloadedEpisode else { return }
        
        if episode.status == DownloadStatus.started.rawValue || episode.status == DownloadStatus.paused.rawValue {
          DownloadManager.shared.cancelDownload()
        }
        
        DownloadManager.shared.delete(with: episode.directoryName!)
      }
      
      let comicToDelete = self.downloadedComics[selectedIndexPath.row]
      
      self.managedContext.delete(comicToDelete)
      self.coreDataStack.saveContext()
      
      UIView.animate(withDuration: 0.3, animations: {
        selectedCell.deleteAllButton.alpha = 0
        selectedCell.deleteAllButton.transform = CGAffineTransform(rotationAngle: .pi / 2)
        selectedCell.deleteAllButton.center.x = selectedCell.deleteDoneButton.center.x
        
        UIView.animate(withDuration: 0.3, animations: {
          selectedCell.deleteDoneButton.alpha = 0
          selectedCell.deleteDoneButton.transform = CGAffineTransform(rotationAngle: .pi / 4)
        }, completion: { _ in
          selectedCell.deleteButton.alpha = 1
          
          self.isDeleteEpisodesButtonActivated = false
          self.fetchDataAndReload()
          
          DownloadManager.shared.checkAndDownload()
        })
      })
    }
  }
  
  private func getCurrentCellIndexPath(sender: Any?) -> IndexPath? {
    if let sourceSender = sender as? UIButton {
      let buttonPosition = sourceSender.convert(CGPoint.zero, to: downloadedComicsTableView)
      let selectedIndexPath = downloadedComicsTableView.indexPathForRow(at: buttonPosition)
      
      return selectedIndexPath
    }
    
    return nil
  }
  
  private func showWarning(with message: String, _ messageColor: UIColor, _ warningColor: UIColor) {
    warningView.alpha = 1
    warningLabel.text = message
    warningLabel.textColor = messageColor
    leftWarningView.backgroundColor = warningColor
    rightWarningView.backgroundColor = warningColor
    
    UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseOut, animations: {
      self.leftWarningView.alpha = 1
      self.rightWarningView.alpha = 1
    }, completion: { _ in
      delay(delay: 1.5) {
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseIn, animations: {
          self.leftWarningView.alpha = 0
          self.rightWarningView.alpha = 0
        }, completion: { _ in
          self.warningView.alpha = 0
        })
      }
    })
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == "ShowDownloadSelection" {
      guard let selectedIndexPath = getCurrentCellIndexPath(sender: sender),
        let selectedCell = downloadedComicsTableView.cellForRow(at: selectedIndexPath) as? DownloadComicTableViewCell,
        let selectedComicID = downloadedComics[selectedIndexPath.row].id,
        let selectedComicTitle = downloadedComics[selectedIndexPath.row].title else { return }
      
      if let destinationViewController = segue.destination as? DownloadSelectionViewController {
        destinationViewController.id = selectedComicID
        destinationViewController.comicTitleFromPreviousVC = selectedComicTitle
        destinationViewController.comicCoverFromPreviousVC = selectedCell.comicCover.image
        destinationViewController.delegate = self
        destinationViewController.managedContext = managedContext
      }
    }
  }
  
  fileprivate func fetchDataAndReload() {
    let downloadedComicFetchRequest: NSFetchRequest<DownloadedComic> = DownloadedComic.fetchRequest()
    
    do {
      let results = try managedContext.fetch(downloadedComicFetchRequest) as [DownloadedComic]
      
      downloadedComics = results
      
      downloadedComicsTableView.reloadData()
      
      if downloadedComics.count == 0 {
        delay(delay: 0.2) {
          self.showWarning(with: "还没有下载动漫", HexColor("#1D33B5")!, HexColor("#4259E7")!)
        }
      }
    } catch let error as NSError {
      print("Unresolved error for getting managed context: \(error), \(error.userInfo)")
    }
  }
  
  @objc func updateProgress(_ notification: Notification) {
    if let directoryName = notification.userInfo?["directoryName"] as? String {
      var stringArray = directoryName.components(separatedBy: " ")
      let index = Int(String(stringArray.last!.dropFirst().dropLast()))!
      
      stringArray.removeLast()
      
      let comicTitle = stringArray.joined(separator: " ")
      
      guard let visibleComicCells = downloadedComicsTableView.visibleCells as? [DownloadComicTableViewCell] else { return }
      
      for visibleComicCell in visibleComicCells {
        if visibleComicCell.comicTitle.text! == comicTitle {
          guard let visibleEpisodeCells = visibleComicCell.downloadedEpisodeCollectionView.visibleCells as? [EpisodeCollectionViewCell] else { return }
          
          let matchedEpisodeCells = visibleEpisodeCells.filter({ $0.downloadedEpisodeButton.title(for: .normal) == String(index) })
          
          if matchedEpisodeCells.count != 0 {
            DispatchQueue.main.async {
              matchedEpisodeCells[0].chartProgress.updateProgress(CGFloat(DownloadManager.shared.downloadingProgress), animated: true, initialDelay: 1.0)
            }
          }
        }
      }
    }
  }
  
  deinit {
    NotificationCenter.default.removeObserver(self)
  }
}

extension DownloadViewController: UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return downloadedComics.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let cell = tableView.dequeueReusableCell(withIdentifier: "DownloadComicCell") as? DownloadComicTableViewCell else {
      fatalError("Cannot find the cell.")
    }
    
    cell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
    
    let cover = UIImage(data: downloadedComics[indexPath.row].cover! as Data)
    cell.comicCover.image = cover
    cell.comicTitle.text = downloadedComics[indexPath.row].title
    cell.rotateAndPositionButton()
    
    tableView.rowHeight = cell.downloadedEpisodeCollectionView.collectionViewLayout.collectionViewContentSize.height + 118
    
    if isDeleteEpisodesButtonActivated {
      cell.deleteButton.alpha = 0
      
      UIView.animate(withDuration: 0.3, animations: {
        cell.deleteDoneButton.alpha = 1
        cell.deleteDoneButton.transform = .identity
        
        UIView.animate(withDuration: 0.3, animations: {
          cell.deleteAllButton.alpha = 1
          cell.deleteAllButton.transform = .identity
          cell.deleteAllButtonConstraint.constant = 60
          
          self.view.layoutIfNeeded()
        })
      })
    } else {
      cell.deleteAllButton.center.x = cell.deleteButton.center.x
    }
    
    return cell
  }
}

extension DownloadViewController: UITableViewDelegate {
  func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    guard let downloadeComicCell = cell as? DownloadComicTableViewCell else { return }
    
    
    downloadeComicCell.collectionViewOffset = storedOffsets[indexPath.row] ?? 0
  }
  
  func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    guard let downloadeComicCell = cell as? DownloadComicTableViewCell else { return }
    
    storedOffsets[indexPath.row] = downloadeComicCell.collectionViewOffset
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: false)
  }
}

extension DownloadViewController: UICollectionViewDataSource {
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    guard let count = downloadedComics[collectionView.tag].episodes?.count else { return 0 }
    
    return count
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EpisodeCell", for: indexPath) as? EpisodeCollectionViewCell else {
      fatalError("Cannot find the cell")
    }
    
    let sortDescriptor = NSSortDescriptor(key: "key", ascending: true)
    let sortedEpisodes = downloadedComics[collectionView.tag].episodes?.sortedArray(using: [sortDescriptor])
    
    guard let episode = sortedEpisodes?[indexPath.row] as? DownloadedEpisode else { fatalError("Cannot find episodes.") }
    
    let episodeNumber = episode.key
    
    cell.setup(number: indexPath.row, episodeNumber: episodeNumber, with: "FromDownloadViewController")
    cell.chartProgress.updateProgress(CGFloat(episode.progress), animated: false, initialDelay: 0)
    
    if isDeleteEpisodesButtonActivated {
      DispatchQueue.main.async {
        cell.deleteOverlayButton.alpha = 1
      }
    } else {
      DispatchQueue.main.async {
        cell.deleteOverlayButton.alpha = 0
      }
    }
    
    return cell
  }
}

extension DownloadViewController: UICollectionViewDelegate {
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    guard let cell = collectionView.cellForItem(at: indexPath) as? EpisodeCollectionViewCell else { return }
    
    guard let episodesCount = downloadedComics[collectionView.tag].episodes?.count else { return }
    
    for i in 0 ..< episodesCount {
      guard let episode = self.downloadedComics[collectionView.tag].episodes?[i] as? DownloadedEpisode else { return }
      
      if episode.key == Int16(cell.downloadedEpisodeButton.title(for: .normal)!)! - 1 {
        if isDeleteEpisodesButtonActivated {
          if episode.status == DownloadStatus.started.rawValue || episode.status == DownloadStatus.paused.rawValue {
            DownloadManager.shared.cancelDownload()
            DownloadManager.shared.downloadingProgress = 0.0
          }
          
          DownloadManager.shared.delete(with: episode.directoryName!)
          
          managedContext.delete(episode)
          coreDataStack.saveContext()
          
          collectionView.reloadData()
          
          let downloadedComicFetchRequest: NSFetchRequest<DownloadedComic> = DownloadedComic.fetchRequest()
          downloadedComicFetchRequest.predicate = NSPredicate(format: "id = %@", downloadedComics[collectionView.tag].id!)
          
          do {
            let result = (try managedContext.fetch(downloadedComicFetchRequest) as [DownloadedComic]).first
            
            if (result?.episodes?.count)! == 0 {
              managedContext.delete(downloadedComics[collectionView.tag])
              coreDataStack.saveContext()
              
              DownloadManager.shared.downloadingProgress = 0
            }
          } catch let error as NSError {
            print("Unresolved error for saving to managed context: \(error), \(error.userInfo)")
          }
        } else {
          if episode.status == DownloadStatus.readyToStart.rawValue {
            DownloadManager.shared.checkAndDownload()
          } else if episode.status == DownloadStatus.started.rawValue {
            cell.pauseView.alpha = 1
            
            DownloadManager.shared.pauseAllDownload()
          } else if episode.status == DownloadStatus.paused.rawValue {
            cell.pauseView.alpha = 0
            
            DownloadManager.shared.resumeDownload()
          } else if episode.status == DownloadStatus.finished.rawValue {
            let name = episode.directoryName!
            let playerViewController = self.storyboard?.instantiateViewController(withIdentifier: "DownloadedEpisodePlayingViewController") as! PlayerViewController
            
            self.present(playerViewController, animated: true)
            
            server = GCDWebDAVServer(uploadDirectory: getDocumentsDirectory().appendingPathComponent("Downloads").appendingPathComponent(name).path)
            server?.start()
            
            let playingURL = ("http://127.0.0.1:8080/\(name).m3u8").addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            let resource = BMPlayerResource(url: URL(string: playingURL)!, name: name)
            playerViewController.player.setVideo(resource: resource)
          }
        }
      }
    }
  }
}

extension DownloadViewController: DownloadSelectionViewControllerDelegate {
  func addDownload() {
    fetchDataAndReload()
  }
}
