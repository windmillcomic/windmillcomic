//
//  DownloadViewController.swift
//  WindmillComic
//
//  Created by Ziyi Zhang on 15/05/2017.
//  Copyright © 2017 Ziyideas. All rights reserved.
//

import UIKit
import CoreData
import Foundation

import ChameleonFramework
import Spring
import LemonDeer

protocol DownloadSelectionViewControllerDelegate: class {
  func addDownload()
}

class DownloadSelectionViewController: UIViewController {
  @IBAction func cancelButtonDidPressed(_ sender: Any) {
    self.dismiss(animated: true)
  }
  
  var id = ""
  var comicTitleFromPreviousVC = ""
  var comicCoverFromPreviousVC: UIImage?
  
  lazy var coreDataStack = CoreDataStack(modelName: "Comics")
  var managedContext: NSManagedObjectContext!
  var selectedComic: DownloadedComic?

  @IBOutlet var comicBackgroundView: UIView!
  @IBOutlet var comicCover: UIImageView!
  @IBOutlet var comicTitle: UILabel!
  @IBOutlet var downloadAllSwitch: UISwitch!
  @IBOutlet var episodeCollectionView: UICollectionView!
  @IBOutlet var backgroundViewHeightConstraint: NSLayoutConstraint!
  @IBOutlet var buttonBackgroundView: DesignableView!
  
  fileprivate var episodeDetailViewModel = EpisodeDetailViewModel()
  fileprivate var tempSelectedEpisodes = [(key: Int16, detail: EpisodeDetail)]()
  
  weak var delegate: DownloadSelectionViewControllerDelegate?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    downloadAllSwitch.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
    
    comicTitle.text = comicTitleFromPreviousVC
    comicCover.image = comicCoverFromPreviousVC
    
    episodeDetailViewModel.getEpisodes(with: id) {
      self.episodeCollectionView.reloadData()
      
      self.backgroundViewHeightConstraint.constant = 8 + self.comicBackgroundView.frame.height + 12 + self.episodeCollectionView.collectionViewLayout.collectionViewContentSize.height + 12 + self.buttonBackgroundView.frame.height
    }
  }
  
  @IBAction func downloadAllSwitchDidPressed(_ sender: Any) {
    tempSelectedEpisodes = []
    
    if downloadAllSwitch.isOn && episodeDetailViewModel.episodes.count > 0 {
      for i in 0 ..< episodeDetailViewModel.episodes.count {
        guard let cell = episodeCollectionView.cellForItem(at: IndexPath(item: i, section: 0)) as? EpisodeCollectionViewCell else { return }
        cell.episodeDownloadSelectionButton.backgroundColor = HexColor("#D19CFF")
        cell.episodeDownloadSelectionButton.borderColor = HexColor("#D19CFF")!
        cell.episodeDownloadSelectionButton.setTitleColor(UIColor.white, for: .normal)
        
        tempSelectedEpisodes.append((Int16(i), episodeDetailViewModel.episodes[i]))
      }
    } else {
      for i in 0 ..< episodeDetailViewModel.episodes.count {
        guard let cell = episodeCollectionView.cellForItem(at: IndexPath(item: i, section: 0)) as? EpisodeCollectionViewCell else { return }
        cell.episodeDownloadSelectionButton.backgroundColor = UIColor.white
        cell.episodeDownloadSelectionButton.borderColor = HexColor("#D19CFF")!
        cell.episodeDownloadSelectionButton.setTitleColor(HexColor("#D19CFF"), for: .normal)
      }
      
      tempSelectedEpisodes.removeAll()
    }
  }

  @IBAction func backButtonDidPressed(_ sender: Any) {
    self.dismiss(animated: true)
  }

  @IBAction func downloadButtonDidPressed(_ sender: Any) {
    // Fetch corresponding comic, if not exist, create one
    let downloadedComicFetchRequest: NSFetchRequest<DownloadedComic> = DownloadedComic.fetchRequest()
    downloadedComicFetchRequest.predicate = NSPredicate(format: "title = %@", comicTitleFromPreviousVC)
    
    do {
      let results = (try managedContext.fetch(downloadedComicFetchRequest) as [DownloadedComic])
      
      if results.count == 0 {
        let entity = NSEntityDescription.entity(forEntityName: "DownloadedComic", in: managedContext)!
        selectedComic = DownloadedComic(entity: entity, insertInto: managedContext)
        selectedComic?.id = id
        
        if let image = comicCoverFromPreviousVC {
          let coverData = NSData(data: UIImagePNGRepresentation(image)!)
          selectedComic?.cover = coverData as Data
        }
        
        selectedComic?.title = comicTitleFromPreviousVC
        
        coreDataStack.saveContext()
      } else {
        selectedComic = results.first
      }
    } catch let error {
      print(error.localizedDescription)
    }
    
    // if saved episodes exist, check if tempSelectedEpisodes includes
    if (selectedComic?.episodes?.count)! != 0 {
      for i in 0 ..< (selectedComic?.episodes?.count)! {
        guard let episode = selectedComic?.episodes?[i] as? DownloadedEpisode else { return }
        
        tempSelectedEpisodes = tempSelectedEpisodes.filter { $0.key != episode.key }
      }
    }
    
    for i in 0 ..< tempSelectedEpisodes.count {
      _ = NetworkService.getJson(with: Constants.Base + Constants.Video, parameters: [
        "m3u8": tempSelectedEpisodes[i].detail.m3u8!,
        "url": tempSelectedEpisodes[i].detail.playURL!
        ]).then { dict -> Void in
          guard let m3u8ResponseURL = dict["url"] as? String else { return }
          
          let entity = NSEntityDescription.entity(forEntityName: "DownloadedEpisode", in: self.managedContext)!
          let downloadedEpisode = DownloadedEpisode(entity: entity, insertInto: self.managedContext)
          downloadedEpisode.key = self.tempSelectedEpisodes[i].key
          downloadedEpisode.progress = 0.0
          downloadedEpisode.status = DownloadStatus.readyToStart.rawValue
          downloadedEpisode.directoryName = "\(self.comicTitleFromPreviousVC) 第\(self.tempSelectedEpisodes[i].key + 1)集"
          downloadedEpisode.playURL = self.tempSelectedEpisodes[i].detail.playURL!
          downloadedEpisode.m3u8RequestURL = self.tempSelectedEpisodes[i].detail.m3u8!
          downloadedEpisode.m3u8ResponseURL = m3u8ResponseURL
          
          self.selectedComic?.addToEpisodes(downloadedEpisode)
          
          self.coreDataStack.saveContext()
          
          self.delegate?.addDownload()
          
          DownloadManager.shared.checkAndDownload()
      }
    }
    
    self.dismiss(animated: true)
  }
}

extension DownloadSelectionViewController: UICollectionViewDataSource {
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return episodeDetailViewModel.episodes.count
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    return episodeDetailViewModel.cellInstance(collectionView, cellForItemAt: indexPath, with: "FromDownloadSelectionViewController")
  }
}

extension DownloadSelectionViewController: UICollectionViewDelegate {
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    guard let cell = episodeCollectionView.cellForItem(at: indexPath) as? EpisodeCollectionViewCell else { return }

    if cell.episodeDownloadSelectionButton.backgroundColor == nil || cell.episodeDownloadSelectionButton.backgroundColor == UIColor.white {
      cell.episodeDownloadSelectionButton.backgroundColor = HexColor("#D19CFF")
      cell.episodeDownloadSelectionButton.borderColor = HexColor("#D19CFF")!
      cell.episodeDownloadSelectionButton.setTitleColor(UIColor.white, for: .normal)
      
      tempSelectedEpisodes.append((Int16(indexPath.row), episodeDetailViewModel.episodes[indexPath.row]))
      tempSelectedEpisodes = tempSelectedEpisodes.sorted { $0.key < $1.key }
    } else {
      cell.episodeDownloadSelectionButton.backgroundColor = UIColor.white
      cell.episodeDownloadSelectionButton.borderColor = HexColor("#D19CFF")!
      cell.episodeDownloadSelectionButton.setTitleColor(HexColor("#D19CFF"), for: .normal)
      
      tempSelectedEpisodes = tempSelectedEpisodes.filter { $0.key != Int16(indexPath.row) }.sorted { $0.key < $1.key }
    }
  }
}
