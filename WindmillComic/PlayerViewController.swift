//
//  PlayerViewController.swift
//  WindmillComic
//
//  Created by Ziyi Zhang on 28/06/2017.
//  Copyright © 2017 Ziyideas. All rights reserved.
//

import UIKit

import BMPlayer
import ChameleonFramework

class PlayerViewController: UIViewController {

  @IBOutlet var player: BMPlayer!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    BMPlayerConf.loaderType = .ballGridBeat
    BMPlayerConf.tintColor = HexColor("#6B7EE9")!
    
    player.backBlock = { isFullScreen in
      if !isFullScreen {
        self.dismiss(animated: true)
      }
    }
  }

}
