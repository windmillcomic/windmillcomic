//
//  DownloadManager.swift
//  WindmillComic
//
//  Created by Ziyi Zhang on 21/06/2017.
//  Copyright © 2017 Ziyideas. All rights reserved.
//

import Foundation

import LemonDeer
import CoreData

enum DownloadStatus: Int16 {
  case readyToStart = 0, started, paused, finished, failed
}

class DownloadManager {
  lazy var coreDataStack = CoreDataStack(modelName: "Comics")
  var managedContext: NSManagedObjectContext!
  
  static let shared = DownloadManager()
  
  let lemonDeer = LemonDeer()
  
  var downloadingProgress: Float = 0.0
  
  var downloadedComics: [DownloadedComic] {
    let downloadedComicFetchRequest: NSFetchRequest<DownloadedComic> = DownloadedComic.fetchRequest()

    do {
      let results = try managedContext.fetch(downloadedComicFetchRequest) as [DownloadedComic]
      
      if results.count > 0 {
        return results
      }
    } catch let error {
      print(error.localizedDescription)
    }
    
    return []
  }
  
  func checkAndDownload() {
    if downloadedComics.count > 0 {
      for i in 0 ..< downloadedComics.count {
        guard let episodes = downloadedComics[i].episodes else { return }
        
        for j in 0 ..< episodes.count {
          guard let episode = episodes[j] as? DownloadedEpisode else { return }
          
          if episode.status == DownloadStatus.started.rawValue { return }
        }
      }
      
      for i in 0 ..< downloadedComics.count {
        guard let episodes = downloadedComics[i].episodes else { return }
        
        for j in 0 ..< episodes.count {
          guard let episode = episodes[j] as? DownloadedEpisode else { return }
          
          if episode.status == DownloadStatus.readyToStart.rawValue || episode.status == DownloadStatus.paused.rawValue {
            episode.status = DownloadStatus.started.rawValue
            
            coreDataStack.saveContext()
            
            lemonDeer.directoryName = episode.directoryName!
            lemonDeer.m3u8URL = episode.m3u8ResponseURL!
            lemonDeer.delegate = self
            lemonDeer.parse()
            
            return
          }
        }
      }
    }
  }
  
  func pauseAllDownload() {
    lemonDeer.downloader.pauseDownloadSegment()
    
    if downloadedComics.count > 0 {
      for i in 0 ..< downloadedComics.count {
        guard let episodes = downloadedComics[i].episodes else { return }
        
        for j in 0 ..< episodes.count {
          guard let episode = episodes[j] as? DownloadedEpisode else { return }
          
          if episode.status == DownloadStatus.started.rawValue {
            episode.status = DownloadStatus.paused.rawValue
            
            coreDataStack.saveContext()
          }
        }
      }
    }
  }
  
  func resumeDownload() {
    lemonDeer.downloader.resumeDownloadSegment()
    
    if downloadedComics.count > 0 {
      for i in 0 ..< downloadedComics.count {
        guard let episodes = downloadedComics[i].episodes else { return }
        
        for j in 0 ..< episodes.count {
          guard let episode = episodes[j] as? DownloadedEpisode else { return }
          
          if episode.status == DownloadStatus.paused.rawValue {
            episode.status = DownloadStatus.started.rawValue
            
            coreDataStack.saveContext()
          }
        }
      }
    }
  }
  
  func cancelDownload() {
    lemonDeer.downloader.cancelDownloadSegment()
    
    if downloadedComics.count > 0 {
      for i in 0 ..< downloadedComics.count {
        guard let episodes = downloadedComics[i].episodes else { return }
        
        for j in 0 ..< episodes.count {
          guard let episode = episodes[j] as? DownloadedEpisode else { return }
          
          if episode.status == DownloadStatus.paused.rawValue || episode.status == DownloadStatus.started.rawValue {
            episode.progress = 0.0
            
            managedContext.delete(episode)
            
            coreDataStack.saveContext()
          }
        }
      }
    }
  }
  
  func delete(with directoryName: String) {
    lemonDeer.downloader.deleteDownloadedContents(with: directoryName)
    
    downloadingProgress = 0
  }
}

extension DownloadManager: LemonDeerDelegate {
  func videoDownloadSucceeded() {
    checkAndDownload()
  }
  
  func videoDownloadFailed() {
    print("Video download failed.")
  }
  
  func update(_ progress: Float, with directoryName: String) {
    for i in 0 ..< downloadedComics.count {
      for j in 0 ..< (downloadedComics[i].episodes?.count)! {
        guard let episode = downloadedComics[i].episodes?[j] as? DownloadedEpisode else { return }
        
        if episode.status == DownloadStatus.started.rawValue {
          episode.progress = progress
        }
        
        if episode.progress == 1.0 {
          episode.status = DownloadStatus.finished.rawValue
        }
        
        coreDataStack.saveContext()
      }
    }
    
    downloadingProgress = progress
    
    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UpdateDownloadProgress"), object: nil, userInfo: [
      "directoryName": directoryName
    ])
  }
}
