//
//  ComicOverview.swift
//  WindmillComic
//
//  Created by Ziyi Zhang on 17/05/2017.
//  Copyright © 2017 Ziyideas. All rights reserved.
//

import Foundation

import Gloss

struct ComicOverView: JSONDecodable {
  let id: String?
  let title: String?
  let coverURL: String?
  let year: String?
  let totalEpisodes: String?
  
  init?(json: JSON) {    
    self.id = "id" <~~ json
    self.title = "title" <~~ json
    self.coverURL = "img" <~~ json
    self.year = "year" <~~ json
    self.totalEpisodes = "total" <~~ json
  }
}
