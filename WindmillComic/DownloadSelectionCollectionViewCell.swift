//
//  DownloadSelectionCollectionViewCell.swift
//  WindmillComic
//
//  Created by Ziyi Zhang on 23/05/2017.
//  Copyright © 2017 Ziyideas. All rights reserved.
//

import UIKit

import Spring

class DownloadSelectionCollectionViewCell: UICollectionViewCell {
  
  
  func setup(number: Int, episode: EpisodeDetail) {
    episodeButton.setTitle("\(number + 1)", for: .normal)
  }
}
