//
//  ComicViewModel.swift
//  WindmillComic
//
//  Created by Ziyi Zhang on 17/05/2017.
//  Copyright © 2017 Ziyideas. All rights reserved.
//

import UIKit
import CoreData

import Alamofire
import Gloss
import PromiseKit

class ComicOverviewViewModel: CellRepresentable {
  var comicOverviews = [ComicOverView]()
  var searchedComicOverviews = [ComicOverView]()
  var comicManagedObjects = [StaredComic]()
  var staredComicsShowed = false
  var searchedComicsShowed = false
  var managedContext: NSManagedObjectContext!
  
  func getComics(on page: Int = 1, completion: @escaping () -> Void) {
    _ = NetworkService.getJson(with: Constants.Base + Constants.Newest + "\(page)").then { dict -> Void in
      let dictsJson = dict["list"] as! [JSON]
      
      guard let comics = [ComicOverView].from(jsonArray: dictsJson) else { return }
      
      self.comicOverviews.append(contentsOf: comics)
      
      completion()
    }
  }
  
  func getSearchedComics(with title: String, completion: @escaping () -> Void) {
    searchedComicOverviews.removeAll()
    
    let queryURL = (Constants.Base + Constants.Search + "\(title)").addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
    
    _ = NetworkService.getJson(with: queryURL).then { dict -> Void in
      let dictsJson = dict["list"] as! [JSON]
      
      guard let comics = [ComicOverView].from(jsonArray: dictsJson) else { return }
      
      self.searchedComicOverviews.append(contentsOf: comics)
      
      completion()
    }
  }
  
  func cellInstance(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ComicCell", for: indexPath) as? ComicCollectionViewCell else {
      fatalError("Cannot find the cell")
    }
    
    if staredComicsShowed {
      cell.setup(comicManagedObject: comicManagedObjects[indexPath.row])
      cell.isStared = true
    } else if searchedComicsShowed {
      cell.setup(comicOverview: searchedComicOverviews[indexPath.row])
    } else {
      cell.setup(comicOverview: comicOverviews[indexPath.row])
      
      let starMatchResult = comicManagedObjects.filter { $0.id == comicOverviews[indexPath.row].id }
      
      if starMatchResult.count > 0 {
        cell.isStared = true
      } else {
        cell.isStared = false
      }
    }

    return cell
  }
}
