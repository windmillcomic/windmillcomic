//
//  ComicDetailViewModel.swift
//  WindmillComic
//
//  Created by Ziyi Zhang on 17/05/2017.
//  Copyright © 2017 Ziyideas. All rights reserved.
//

import UIKit

import Alamofire
import Gloss
import PromiseKit

class EpisodeDetailViewModel: CellRepresentable {
  var episodes = [EpisodeDetail]()
  
  func getEpisodes(with ID: String, completion: @escaping () -> Void) {
    _ = NetworkService.getJson(with: Constants.Base + Constants.Detail,
                               parameters: ["iid": ID]).then { dicts -> Void in
                                let dictsJson = dicts["list"] as! [JSON]
                                
//                                guard let episodes = [EpisodeDetail].from(jsonArray: dictsJson) else { return }
                                guard let episodes = [EpisodeDetail].from(jsonArray: dictsJson) else { return }
                                
                                self.episodes.append(contentsOf: episodes)
                                
                                completion()
    }
  }
  
  func cellInstance(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath, with identifier: String) -> UICollectionViewCell {
    guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EpisodeCell", for: indexPath) as? EpisodeCollectionViewCell else {
      fatalError("Cannot find the cell")
    }
    
    cell.setup(number: indexPath.row, with: identifier)
    
    return cell
  }
}
