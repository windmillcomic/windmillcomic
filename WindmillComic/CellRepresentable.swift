//
//  CellRepresentable.swift
//  WindmillComic
//
//  Created by Ziyi Zhang on 17/05/2017.
//  Copyright © 2017 Ziyideas. All rights reserved.
//

import UIKit

@objc protocol CellRepresentable {
  @objc optional func cellInstance(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell
  @objc optional func cellInstance(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath, with identifier: String) -> UICollectionViewCell
}
