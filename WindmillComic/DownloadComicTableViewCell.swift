//
//  DownloadComicTableViewCell.swift
//  WindmillComic
//
//  Created by Ziyi Zhang on 16/05/2017.
//  Copyright © 2017 Ziyideas. All rights reserved.
//

import UIKit

import Spring

class DownloadComicTableViewCell: UITableViewCell {
  @IBOutlet var comicCover: DesignableImageView!
  @IBOutlet var comicTitle: UILabel!
  
  @IBOutlet weak var deleteButton: UIButton!
  @IBOutlet weak var deleteAllButton: SpringButton!
  @IBOutlet weak var deleteDoneButton: SpringButton!
  
  @IBOutlet weak var deleteAllButtonConstraint: NSLayoutConstraint!
  @IBOutlet weak var downloadedEpisodeCollectionView: UICollectionView!
  
  func rotateAndPositionButton() {
    deleteDoneButton.transform = CGAffineTransform(rotationAngle: .pi / 4)
    deleteAllButton.transform = CGAffineTransform(rotationAngle: .pi / 2)
    deleteDoneButton.center.x = deleteButton.center.x
    deleteAllButton.center.x = deleteButton.center.x
  }
}

extension DownloadComicTableViewCell {
  func setCollectionViewDataSourceDelegate<D: UICollectionViewDataSource & UICollectionViewDelegate>(_ dataSourceDelegate: D, forRow row: Int) {
    downloadedEpisodeCollectionView.delegate = dataSourceDelegate
    downloadedEpisodeCollectionView.dataSource = dataSourceDelegate
    downloadedEpisodeCollectionView.tag = row
    downloadedEpisodeCollectionView.setContentOffset(downloadedEpisodeCollectionView.contentOffset, animated:false) // Stops collection view if it was scrolling.
    downloadedEpisodeCollectionView.reloadData()
  }
  
  var collectionViewOffset: CGFloat {
    set { downloadedEpisodeCollectionView.contentOffset.x = newValue }
    get { return downloadedEpisodeCollectionView.contentOffset.x }
  }
}

