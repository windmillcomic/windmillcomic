//
//  Helper.swift
//  WindmillComic
//
//  Created by Ziyi Zhang on 17/05/2017.
//  Copyright © 2017 Ziyideas. All rights reserved.
//

import UIKit

public func localize(with key: String) -> String {
  return NSLocalizedString(key, comment: "")
}

public func localizedFormat(with key: String, and argument: String) -> String {
  return String(format: localize(with: key), argument)
}

extension UIColor {
  public class func colorWithRGB(red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat) -> UIColor {
    return UIColor(red: red/255, green: green/255, blue: blue/255, alpha: alpha)
  }
}

extension UIViewController {
  func alert(message: String, title: String, confirmActionTitle: String, confirmStyle: UIAlertActionStyle, confirmeCompletionHandler: @escaping () -> Void) {
    let alertViewController = UIAlertController(title: title, message: message, preferredStyle: .alert)
    let confirmAction = UIAlertAction(title: confirmActionTitle, style: confirmStyle) { _ in
      confirmeCompletionHandler()
    }
    let cancelAction = UIAlertAction(title: "取消", style: .cancel)
    
    alertViewController.addAction(confirmAction)
    alertViewController.addAction(cancelAction)
    
    self.present(alertViewController, animated: true)
  }
}
