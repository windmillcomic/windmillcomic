//
//  ComicDetail.swift
//  WindmillComic
//
//  Created by Ziyi Zhang on 17/05/2017.
//  Copyright © 2017 Ziyideas. All rights reserved.
//

import Gloss

struct EpisodeDetail: JSONDecodable {
  let playURL: String?
  let m3u8: String?
  
  init?(json: JSON) {
    self.playURL = "bofang" <~~ json
    self.m3u8 = "m3u8" <~~ json
  }
}
